package com.gl;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 *  @author gaolei
 */
@EnableZuulProxy
@SpringCloudApplication
public class ApiGatewagApplication {

    public static void main( String[] args ) {
        new SpringApplicationBuilder(ApiGatewagApplication.class).web(true).run(args);
    }
}
