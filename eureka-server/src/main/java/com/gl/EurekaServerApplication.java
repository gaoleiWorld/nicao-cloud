package com.gl;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 *  @author gaolei
 */
@EnableEurekaServer //启动一个注册服务中心
@SpringBootApplication
public class EurekaServerApplication {

    public static void main( String[] args ) {
        new SpringApplicationBuilder(EurekaServerApplication.class).web(true).run(args);
    }
}
